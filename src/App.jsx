import {BrowserRouter, Route, Routes} from 'react-router-dom'
import Login from './components/Login';
import Admin from './components/Admin'
import Inicio from './components/Inicio';
import Navbar from './components/Navbar'

import {auth} from './firebase'
import { useState } from 'react';
import { useEffect } from 'react';


function App() {
  

    const [firebaseUser, setFirebaseUser] = useState(false)

    useEffect(() => {
      auth.onAuthStateChanged(user => {
        console.log(user)
        if(user){
          setFirebaseUser(user)
        } else {
          setFirebaseUser(null)
        }
      })
    }, [])

    return firebaseUser !== false ? ( 
      <BrowserRouter>
      <div className="container">
        <Navbar firebaseUser={ firebaseUser }/>
          <Routes>
            <Route path="login" element={<Login />}></Route>
            <Route path="admin" element={<Admin />}></Route>
            <Route path="/" element={<Inicio />}></Route>
          </Routes>

      </div>
      
      </BrowserRouter>
      )
      :
      (
        <p>Loading....</p>
      )
}

export default App;

import React from 'react'
import {Link, NavLink, useNavigate} from 'react-router-dom'
import {auth} from '../firebase'

const Navbar = (props) => {

    let navigate = useNavigate()    

    const cerrarSesion = () => {
        auth.signOut()
            .then(() => {
                navigate('/login')
            })
    }


    return (
        <div className="navbar navbar-dark bg-dark container-fluid">
            <Link to="/" className="navbar-brand">React Admin</Link>
            <div>
                <div className="d-flex">
                    <NavLink 
                        className="btn btn-dark mr-2" 
                        to="/"   
                    >
                        Inicio
                    </NavLink>
                    {
                        props.firebaseUser !== null ? (
                            <NavLink 
                                className="btn btn-dark mr-2" 
                                to="/admin">
                                Admin
                            </NavLink>
                        ) : ( null )
                    }
                    
                    {
                        props.firebaseUser !== null ? (
                            <button 
                                className="btn btn-dark"
                                onClick = {()=> cerrarSesion()}>
                                Cerrar Sesión
                            </button>
                            ) : (
                            <NavLink 
                                className="btn btn-dark" 
                                to="/login">
                                Login
                            </NavLink>
                            )
                    }
                    
                </div>
            </div>
        </div>
    )
}

export default Navbar
import React, {useState} from 'react'
import {auth, db} from '../firebase'
import { useNavigate } from "react-router-dom";

const Login = () => {

    const [email, setEmail] = useState('')
    const [pass, setPass] = useState('')
    const [error, setError] = useState(null)

    const [esRegistro, setEsRegistro] = useState(true)

    let navegar = useNavigate()


    const procesarDatos = (e) => {
        e.preventDefault()
        if(!email.trim()){
            console.log('Ingrese Email')
            setError('Ingrese Email')
            return
        } //validacion por si quiero submitiar un campo vacio

        if(!pass.trim()){
            console.log('Ingrese Pass')
            setError('Ingrese Pass')
            return
        }

        if(pass.length < 6){
            console.log('Ingrese Pass mayor a 6 caracteres')
            setError('Pass debe ser mayor a 6 caracs')
            return //ESTO ES UN EJEMPLO PORQUE FIREBASE ME EXIGE ESA LONGITUD
        }


        setError(null)
        console.log('Pasando todas las validaciones')

        if(esRegistro){
            registrarUser()
        } else {
            loginUser()
        }


    }


    const registrarUser = React.useCallback(async() => {

        try{
            const res = await auth.createUserWithEmailAndPassword(email, pass)
            await db.collection('users').doc(res.user.email).set({
                email: res.user.email,
                uid: res.user.uid
            }) //le damos el id a la coleccion que recibimos del usuario registrado

            await db.collection(res.user.uid).add({
                name: 'Tarea de ejemplo',
                fecha: Date.now()
            })

            console.log(res)
            setEmail('')
            setPass('')
            setError(null)

            
            navegar('/admin')
        }catch (error) {
            console.log(error)
            setError(error.message)
        }

    }, [email, pass, navegar])



    const loginUser = React.useCallback(async() => {

        try{
            const res = await auth.signInWithEmailAndPassword(email, pass)
            console.log(res)
            setEmail('')
            setPass('')
            setError(null)

            navegar('/admin')
        }catch (error) {
            console.log(error)
            setError(error.message)
        }

    }, [email, pass, navegar])




    return (
        <div className="mt-5">
            <h3 className="text-center">
                {
                    esRegistro ? 'Registro de usuarios' : 'Login de acceso'
                }
            </h3>
            <hr/>
            <div className="row justify-content-center">
                <div className="col-12 col-sm-8 col-md-6 col-xl-4">
                    <form onSubmit={procesarDatos}>
                        {
                            error && (
                                <div className="alert alert-danger">
                                    {error}
                                </div>
                            )
                        }
                        <input 
                            type="email" 
                            className="form-control mb-2"
                            placeholder="Ingrese Email"
                            onChange = {e => setEmail(e.target.value)}
                            value = {email}
                        />
                        <input 
                            type="password" 
                            className="form-control mb-2"
                            placeholder="Ingrese Contraseña"
                            onChange = {e => setPass(e.target.value)}
                            value = {pass}
                        />
                        <button 
                            className="btn btn-lg btn-dark w-100 m-1"
                            type="submit"
                        >
                            {esRegistro ? 'Registrarse' : 'Acceder'}
                        </button>
                        <button 
                            className="btn btn-sm btn-info w-100 m-1"
                            type="button"
                            onClick={()=> setEsRegistro(!esRegistro)} //Si es verdadero va a pasar a falso y viceversa
                        >
                            {esRegistro ? '¿Ya estás registrado?' : '¿No tienes cuenta?'}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Login
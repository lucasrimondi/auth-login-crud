
import app from "firebase/app";
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
  apiKey: "AIzaSyAEbq3d5OpACaDwGEc3ekxq8cA1IBev7Sg",
  authDomain: "auth-rutas.firebaseapp.com",
  projectId: "auth-rutas",
  storageBucket: "auth-rutas.appspot.com",
  messagingSenderId: "186383957523",
  appId: "1:186383957523:web:2491dce05568e7792609e6"
};

app.initializeApp(firebaseConfig);

const db = app.firestore()
const auth = app.auth()

export {db, auth}
import React, {useEffect, useState} from 'react'
import {auth} from '../firebase'
import { useNavigate } from "react-router-dom";
import Firestore from './Firestore'

const Admin = () => {

    let navegar = useNavigate()

    const [user, setUser] = useState(null)


    useEffect(() => {
        if(auth.currentUser){
            console.log('existe un usuario')
            setUser(auth.currentUser)
        }else{
            console.log('no exixte usuario')
            navegar('/login')
        }
    }, [navegar])


    return (
        <div className='container'>
           
           {
               user && ( //esto es un equivalente a poner que user es !== a null
                   <Firestore user={user}/>
               )
           }
        </div>
    )
}

export default Admin
